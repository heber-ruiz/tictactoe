onmessage = function (oEvent) {
    setInterval(() => {
        fetch('/api/refresh')
            .then(response => response.json())
            .then(data => {
                postMessage(data);
            });
    }, 1000);

  };