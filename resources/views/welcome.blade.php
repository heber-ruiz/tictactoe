<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TicTacToe</title>
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
    {{-- pantalla inicial --}}
    <div class="container mt-3 mx-auto">
        <div id="gameHome" class="card">
            <div class="card-header">
              Juega TicTacToe
            </div>
            <div class="card-body">
              <a id="newGameSetup" href="#" class="btn btn-primary btn-lg">Nuevo Juego</a>
              <a id="joinGameSetup" href="#" class="btn btn-primary btn-lg">Unirse a Juego</a>
            </div>
        </div>

        {{-- pantalla secundaria de crear partida --}}
        <div id="gameSetupScreen" class="card" style="display:none">
            <div class="card-header">
              Nuevo Juego
            </div>
            <div class="card-body">
                <span>Nombre de usuario:</span> <br>
                <input type="text" name="myName" id="myName" value="Jugador 1" class="my-2"> <br>
                <a id="newGame" href="#" class="btn btn-primary btn-lg">Crear Juego</a>
            </div>
        </div>

        {{-- pantalla secundaria de unirse a partida --}}
        <div id="GameJoinScreen" class="card" style="display:none">
            <div class="card-header">
              Buscar Juego
            </div>
            <div class="card-body">
                <span>Nombre de usuario:</span> <br>
                <input type="text" name="myName2" id="myName2" value="Jugador 2" class="my-2"> <br>
                <span>Codigo de partida:</span> <br>
                <input type="text" name="gameCodeInput" id="gameCodeInput"  class="my-2"> <br>
                <a id="joinGame" href="#" class="btn btn-primary btn-lg">Buscar Juego</a>
            </div>
        </div>
        {{-- Tablero de juego --}}
        <div id="gameBoard" class="card" style="display:none">
            <div class="card-header">
              Codigo a compartir # <strong class="text-success" id="gameCode"></strong> <div class="float-right"> <span id="gameWaiting"></span></div>
            </div>
            <div class="card-body bg-dark">
                <div class="row d-flex justify-content-around align-items-center" style="height: 100px">
                    <span id="player1" class="h3 text-light"></span> <span class="h2 text-light">VS</span> <span id="player2" class="h3 text-light"></span>
                </div>
                <div class="row">
                    <div id="1x1" class="col border d-flex justify-content-center align-items-center h1 bg-light" style="height: 100px"></div>
                    <div id="1x2" class="col border d-flex justify-content-center align-items-center h1 mx-1 bg-light" style="height: 100px"></div>
                    <div id="1x3" class="col border d-flex justify-content-center align-items-center h1 bg-light" style="height: 100px"></div>
                </div>
                <div class="row">
                    <div id="2x1" class="col border d-flex justify-content-center align-items-center h1 bg-light" style="height: 100px"></div>
                    <div id="2x2" class="col border d-flex justify-content-center align-items-center h1 mx-1 bg-light" style="height: 100px"></div>
                    <div id="2x3" class="col border d-flex justify-content-center align-items-center h1 bg-light" style="height: 100px"></div>
                </div>
                <div class="row">
                    <div id="3x1" class="col border d-flex justify-content-center align-items-center h1 bg-light" style="height: 100px"></div>
                    <div id="3x2" class="col border d-flex justify-content-center align-items-center h1 mx-1 bg-light" style="height: 100px"></div>
                    <div id="3x3" class="col border d-flex justify-content-center align-items-center h1 bg-light" style="height: 100px"></div>
                </div>
                <div class="row d-flex justify-content-around align-items-center" style="height: 100px">
                    <span id="bannerFinal" class="h2 text-light"></span>
                    <a id="continueGame" href="#" class="btn btn-primary btn-lg" style="display:none">Continuar</a>
                </div>
            </div>
        </div>
    </div>

    <script>
        // actualiza tableroi de juego
        function executeGame(data){
            document.getElementById("gameCode").textContent = data.code;

            var myWorker = new Worker("js/worker.js");

            //run update logic on worker notification
            myWorker.onmessage = function (oEvent) {
                console.log("Worker said : " + JSON.stringify(oEvent.data));

                document.getElementById("player1").textContent = oEvent.data.myName;
                document.getElementById("player2").textContent = oEvent.data.otherName;

                if(oEvent.data.waiting){
                    document.getElementById("gameWaiting").textContent = 'Esperando turno';
                } else {
                    document.getElementById("gameWaiting").textContent = '';
                }

                var myBinStr = oEvent.data.myLayout.toString(2).padStart(9,'0');
                var otherBinStr = oEvent.data.otherLayout.toString(2).padStart(9,'0');

                var boardIds = ['1x1', '1x2', '1x3', '2x1', '2x2', '2x3', '3x1', '3x2', '3x3'];

                //actualiza  disposiciond e nuestras fichas
                for(var i = 0; i < myBinStr.length; i++){
                    if(myBinStr.charAt(i)=='1'){
                        document.getElementById(boardIds[i]).textContent = oEvent.data.myMark;
                    }
                }

                //actualiza disposicion de fichas del oponente
                for(var i = 0; i < otherBinStr.length; i++){
                    if(otherBinStr.charAt(i)=='1'){
                        document.getElementById(boardIds[i]).textContent = (oEvent.data.myMark == 'x'? 'o': 'x');
                    }
                }

                //limpia el tablero si no hay fichas
                if(oEvent.data.myLayout ==0 && oEvent.data.otherLayout == 0){
                    for(var i = 0; i < boardIds.length; i++){
                        document.getElementById(boardIds[i]).textContent = '';
                    }
                }

                // valida condiciones de victoria
                if(oEvent.data.status == 'tie'){
                    document.getElementById("bannerFinal").textContent = 'Empate!';
                    document.getElementById("continueGame").style.display = "inline"; 

                } else if(oEvent.data.status == 'win'){
                    document.getElementById("bannerFinal").textContent = 'Ganaste!';
                    document.getElementById("continueGame").style.display = "inline"; 
                } else if(oEvent.data.status == 'lose'){
                    document.getElementById("bannerFinal").textContent = 'Perdiste!';
                    document.getElementById("continueGame").style.display = "inline"; 
                }
                else {
                    document.getElementById("bannerFinal").textContent = '';
                    document.getElementById("continueGame").style.display = "none";
                }

            };

            // inicia el webworker
            myWorker.postMessage('');
        }

        //callbacks para los eventos

        document.getElementById("newGameSetup").addEventListener('click', function(){
            document.getElementById("gameHome").style.display = "none";
            document.getElementById("gameSetupScreen").style.display = "block";

        })

        document.getElementById("newGame").addEventListener('click', function(){
            document.getElementById("gameSetupScreen").style.display = "none";
            document.getElementById("gameBoard").style.display = "block";
            var name = document.getElementById("myName").value ; 
            fetch('/api/create?name='+name)
            .then(response => response.json())
            .then(data => {
                executeGame(data);
            });
        })

        document.getElementById("joinGameSetup").addEventListener('click', function(){
            document.getElementById("gameHome").style.display = "none";
            document.getElementById("GameJoinScreen").style.display = "block";

        })

        document.getElementById("joinGame").addEventListener('click', function(){
            document.getElementById("GameJoinScreen").style.display = "none";
            document.getElementById("gameBoard").style.display = "block";
            var name = document.getElementById("myName2").value ; 
            var codigo = document.getElementById("gameCodeInput").value ; 
            fetch('/api/join/'+codigo+'?name='+name)
            .then(response => response.json())
            .then(data => {
                executeGame(data);
            });
        })

        document.getElementById("continueGame").addEventListener('click', function(){
            fetch('/api/continue')
            .then(response => response.json())
            .then(data => {
                var boardIds = ['1x1', '1x2', '1x3', '2x1', '2x2', '2x3', '3x1', '3x2', '3x3'];
                for(var i = 0; i < boardIds.length; i++){
                        document.getElementById(boardIds[i]).textContent = '';
                    }
                executeGame(data);
            });
        })

        //eventos de posicionamiento de fichas en el tablero

        document.getElementById("1x1").addEventListener('click', function(){
            fetch('/api/move?layout=256')
            .then(response => response.json())
            .then(data => {
               console.log(data);
            });
        })

        document.getElementById("1x2").addEventListener('click', function(){
            fetch('/api/move?layout=128')
            .then(response => response.json())
            .then(data => {
               console.log(data);
            });
        })

        document.getElementById("1x3").addEventListener('click', function(){
            fetch('/api/move?layout=64')
            .then(response => response.json())
            .then(data => {
               console.log(data);
            });
        })

        document.getElementById("2x1").addEventListener('click', function(){
            fetch('/api/move?layout=32')
            .then(response => response.json())
            .then(data => {
               console.log(data);
            });
        })

        document.getElementById("2x2").addEventListener('click', function(){
            fetch('/api/move?layout=16')
            .then(response => response.json())
            .then(data => {
               console.log(data);
            });
        })

        document.getElementById("2x3").addEventListener('click', function(){
            fetch('/api/move?layout=8')
            .then(response => response.json())
            .then(data => {
               console.log(data);
            });
        })

        document.getElementById("3x1").addEventListener('click', function(){
            fetch('/api/move?layout=4')
            .then(response => response.json())
            .then(data => {
               console.log(data);
            });
        })

        document.getElementById("3x2").addEventListener('click', function(){
            fetch('/api/move?layout=2')
            .then(response => response.json())
            .then(data => {
               console.log(data);
            });
        })

        document.getElementById("3x3").addEventListener('click', function(){
            fetch('/api/move?layout=1')
            .then(response => response.json())
            .then(data => {
               console.log(data);
            });
        })
        
    </script>
</body>
</html>