<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game; 

class GameController extends Controller
{
    //crea una partida nueva, devuelve el codigo de la partida creada
    public function create(Request $request){
        $name= $request->input('name', 'Jugador 1');

        $code = uniqid();

        $request->session()->put('code', $code);
        $myId = $request->session()->getId();

        $prevHostedGames = Game::where('player1', $myId)->get();
        foreach ($prevHostedGames as $toDelete) {
            $toDelete->delete();
        }

        $prevGuestGames = Game::where('player2', $myId)->get();
        foreach ($prevGuestGames as $toDelete) {
            $toDelete->delete();
        }

        $newGame = new Game;
        $newGame->code = $code;
        $newGame->player1 = $myId;
        $newGame->player1_name = $name;
        $newGame->layout1=0;
        $newGame->turn = $myId;
        $newGame->player2 = null;
        $newGame->player2_name = 'Jugador 2';
        $newGame->layout2 = 0;
        $newGame->status = 'waiting';
        $newGame->save();


        return ['status' => 'ok', 'code' => $code];
    }

    // se une a una partida existente, devuelve el codigo de la partida a la q se unio.
    public function join(Request $request, string $code){
        $name= $request->input('name', 'Jugador 2');

        $request->session()->put('code', $code);
        $myId = $request->session()->getId();

        $prevHostedGames = Game::where('player1', $myId)->get();
        foreach ($prevHostedGames as $toDelete) {
            $toDelete->delete();
        }

        $prevGuestGames = Game::where('player2', $myId)->get();
        foreach ($prevGuestGames as $toDelete) {
            $toDelete->delete();
        }


        $invitedGame = Game::where('code', $code)->first();

        if($invitedGame && $invitedGame->player2 == null){
            $invitedGame->player2 = $myId;
            $invitedGame->player2_name = $name;
            $invitedGame->layout2=0;
            $invitedGame->turn = $myId;
            $invitedGame->status = 'playing';
            $invitedGame->save();

            return ['status' => 'ok', 'code' => $code];

        } else {
            return ['status' => 'Game not found'];
        }

        
    }

    // envia la informacion de las fichas y demas data para popular la interfaz,
    // esta es la funcion q se llama dentro del web worker.
    public function refresh(Request $request){

        $myId = $request->session()->getId();

        $hostedGame = Game::where('player1', $myId)->first();
        $guestGame = Game::where('player2', $myId)->first();


        

        if($hostedGame){
            $gameStatus = $hostedGame->status;
            return [
                'status' => $gameStatus == 'win_player1' ? 'win' : ($gameStatus =='win_player2' ? 'lose': $gameStatus),
                'myName' => $hostedGame->player1_name,
                'otherName' => $hostedGame->player2_name,
                'myLayout' => $hostedGame->layout1,
                'otherLayout' => $hostedGame->layout2,
                'myMark' => 'x',
                'waiting' => ($hostedGame->turn == $myId)
            ];

        } else if($guestGame){
            $gameStatus = $guestGame->status;
            return [
                'status' => $gameStatus == 'win_player2' ? 'win' : ($gameStatus =='win_player1' ? 'lose': $gameStatus),
                'myName' => $guestGame->player2_name,
                'otherName' => $guestGame->player1_name,
                'myLayout' => $guestGame->layout2,
                'otherLayout' => $guestGame->layout1,
                'myMark' => 'o',
                'waiting' => ($guestGame->turn == $myId)
            ];

        } else {
            return [
                'status' => 'Game not found'
            ];

        }
        
    }

    //actualiza las fichas en el tablero, realiza las validaciones para evitar inconsistencias 
    // y reconocer las condiciones de vistoria, perdida o empate.
    public function move(Request $request){

        $layout= $request->input('layout');
        $myId = $request->session()->getId();

        $hostedGame = Game::where('player1', $myId)->first();
        $guestGame = Game::where('player2', $myId)->first();

        if($hostedGame && $hostedGame->turn != $myId && $hostedGame->status == 'playing'){
            $layout = $layout | $hostedGame->layout1;
            if($this->validateLayouts($layout, $hostedGame->layout2)){
                $hostedGame->layout1 = $layout;
                if ($this->validateTie($layout, $hostedGame->layout2)) {
                    $hostedGame->status = 'tie';
                } else if ($this->validateWin($layout)){
                    $hostedGame->status = 'win_player1';
                }
                $hostedGame->turn = $myId;
                $hostedGame->save();
            }

        } else if($guestGame && $guestGame->turn != $myId && $guestGame->status == 'playing'){
            $layout = $layout | $guestGame->layout2;
            if($this->validateLayouts($layout, $guestGame->layout1)){
                $guestGame->layout2 = $layout;
                if ($this->validateTie($layout, $guestGame->layout2)) {
                    $guestGame->status = 'tie';
                } else if ($this->validateWin($layout)){
                    $guestGame->status = 'win_player2';
                }
                $guestGame->turn = $myId;
                $guestGame->save();
            }

        } 
        
        return [
            'status' => 'Ok'
        ];
    }

    // resetea la disposicion de las fichas y realiza los ajustes para alternar los jugadores luego de finalizar una partida.
    public function continue( Request $request){
        $myId = $request->session()->getId();

        $hostedGame = Game::where('player1', $myId)->first();
        $guestGame = Game::where('player2', $myId)->first();

        if($hostedGame){

            $current_name = $hostedGame->player1_name;

            $hostedGame->layout1=0;
            $hostedGame->turn = $myId;
            $hostedGame->player1 = $hostedGame->player2;
            $hostedGame->player1_name = $hostedGame->player2_name;
            $hostedGame->player2 = $myId;
            $hostedGame->player2_name = $current_name;
            $hostedGame->layout2 = 0;
            $hostedGame->status = 'playing';
            $hostedGame->save();


        return ['status' => 'ok', 'code' => $hostedGame->code];

        } else if($guestGame){

            $player1_name = $guestGame->player1_name;
            $player1 = $guestGame->player1;

            $guestGame->layout1=0;
            $guestGame->turn = $player1;
            $guestGame->player1 = $myId;
            $guestGame->player1_name = $guestGame->player2_name;
            $guestGame->player2 = $player1;
            $guestGame->player2_name = $player1_name;
            $guestGame->layout2 = 0;
            $guestGame->status = 'playing';
            $guestGame->save();

            return ['status' => 'ok', 'code' => $guestGame->code];

        } 


    }

    //valida que el posicionamiento de las marcas sea correcto
    private function validateLayouts($player1, $player2){
        return ($player1 & $player2) == 0;
    }

    // valida si hay un empate
    private function validateTie($player1, $player2) {
        return ($player1 | $player2) == 511;
    }

    // valida las condiciones de victoria
    private function validateWin($player){
        if(
            (($player & 7) == 7)   || // bottom row
            (($player & 56) == 56)  || // mid row
            (($player & 448) == 448) || // top row
            (($player & 292) == 292) || // left row
            (($player & 146) == 146) || // mid row
            (($player & 73) == 73)  || // right row
            (($player & 273) == 273) || // left to right diag
            (($player & 84) == 84)     // rigth to left diag
        ){
            return true;
        } else {
            return false;
        }
    }
}
