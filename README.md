

## Instalacion

- clonar el repositorio
- configurar archivo .env con conexion a db mysql
- composer install
- php artisan migrate
- php artisan key:generate
- php artisan serve

## Detalles de implementacion
Gamecontroller contiene las apis para crear partida, unirse a partida, ver datos de partida, actualizar partida y continuar una partida terminada, ademas de algunas funciones de ayuda. Para el front se usa un web worker que hace long polling de la data cada segundo, y para la actualizacion de data se envia en eventos aparte, toda al data se recoge con el web worker y se actualiza la interfaz. La disposicion de las marcas 'x' y 'o' se codifica en binario y se validan en el servidor. El servidor solo trabaja con la disposicion codificada y esta solo se decodifica para mostrarla en la grilla. Para identificar el usuario se utilizan sesiones las cuales se relacionan con la partida en la BD. Una implementacion mas robusta puede utilizar websockets en ves de long polling, y utilizar un cache de memoria en vez de mysql para guardar la data de la partida.
