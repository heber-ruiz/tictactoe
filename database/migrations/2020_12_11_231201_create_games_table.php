<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->enum('status', ['waiting', 'playing', 'tie', 'win_player1', 'win_player2']);
            $table->string('code');
            $table->string('turn')->nullable();
            $table->string('player1')->nullable();
            $table->string('player1_name');
            $table->integer('layout1')->nullable();
            $table->string('player2')->nullable();
            $table->string('player2_name');
            $table->integer('layout2')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
